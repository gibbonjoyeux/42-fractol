--[[
--------------------------------------------------------------------------------
--	by REEZ
--	project name: Fractol
--	file name: draw_cpu.lua
--------------------------------------------------------------------------------
--	description:
--		Draw mandelbrot using CPU.
--------------------------------------------------------------------------------
--]]


function draw_cpu()
	local lg = lg
	local pos_x, pos_y = pos_x, pos_y
	local max_iter = max_iter
	local x, y
	local w, h
	local i
	local zc, zr
	local new_zc, new_zr

	w = WINDOW.sizes[ WINDOW.scale ][ 1 ]
	h = WINDOW.sizes[ WINDOW.scale ][ 2 ]
	lg.setCanvas( c_main )
		lg.clear()
		for y = 0, w do
			for  x = 0, h do
				zc = 0--mouse_x / 700
				zr = 0--mouse_y / 700
				i = 0
				while i < max_iter do
					new_zc = 2 * zc * zr + (y + pos_y) / zoom
					new_zr = zr * zr - zc * zc + (x + pos_x) / zoom
					zc = new_zc
					zr = new_zr
					if ( zc * zc + zr * zr > 4 ) then
						break
					end
					i = i + 1
				end
				if ( i == max_iter ) then
					i = 255
				end
				lg.setColor( i * 10, i * 15, i * i, 255 )
				--lg.setColor( i * math.abs(w / 2 - x) / 5,
				----[[]] i * math.abs(h / 2 - y) / 5, i * i, 255 )
				lg.points( x, y )
			end
		end
		m_lib.fps.print()
		lg.setColor( 0, 0, 0 )
		lg.print( 'zoom: ' .. zoom, 0, 10 )
		lg.print( 'coef: ' .. ( WINDOW_W / zoom ), 0, 20 )
		lg.setColor( 255, 255, 255, 255 )
end
