--[[
--------------------------------------------------------------------------------
--	by REEZ
--	project name: Fractol
--	file name: draw_gpu.lua
--------------------------------------------------------------------------------
--	description:
--		Draw Mandelbrot using GPU.
--------------------------------------------------------------------------------
--]]

function draw_gpu()
	lg.setCanvas( c_main )
		lg.setShader( fractal )
		lg.draw( c_fractal )
		lg.setShader()
		m_lib.fps.print()
end
