--[[
--------------------------------------------------------------------------------
--	by REEZ
--	project name: Fractol
--	file name: load_canvas.lua
--------------------------------------------------------------------------------
--	description:
--		Create every canvas of the project.
--------------------------------------------------------------------------------
--]]

local lg = lg

--------------------------------------------------------------------------------
--[ CANVAS ]--------------------------------------------------------------------
--------------------------------------------------------------------------------

c_main		= lg.newCanvas( WINDOW_W, WINDOW_H )
c_fractal	= lg.newCanvas( WINDOW_W, WINDOW_H )
