/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   set_fractal.c                                      :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: shuertas <marvin@42.fr>                    +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2018/03/06 15:39:44 by shuertas          #+#    #+#             */
/*   Updated: 2018/03/06 15:39:45 by shuertas         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "fractol.h"

/*
********************************************************************************
*** STATIC FUNCTIONS ***********************************************************
********************************************************************************
*/

static void	init_struct(t_fractol *fct)
{
	fct->level = 0;
	fct->pos_x = 0;
	fct->pos_y = 0;
	fct->mouse_x = 0;
	fct->mouse_y = 0;
	fct->domain = 3.5;
	fct->dynamic = 1;
}

/*
********************************************************************************
*** PUBLIC FUNCTION ************************************************************
********************************************************************************
*/

void		set_fractal(t_fractol *fct, uint8_t fractal)
{
	fractal = fractal % 4;
	if (fractal == 0)
	{
		fct->f_init = &calcul_init_mandelbrot;
		fct->f_precise = &calcul_precise_mandelbrot;
	}
	else if (fractal == 1)
	{
		fct->f_init = &calcul_init_julia;
		fct->f_precise = &calcul_precise_julia;
	}
	else if (fractal == 2)
	{
		fct->f_init = &calcul_init_ship;
		fct->f_precise = &calcul_precise_ship;
	}
	else
	{
		fct->f_init = &calcul_init_tunnel;
		fct->f_precise = &calcul_precise_tunnel;
	}
	init_struct(fct);
}
