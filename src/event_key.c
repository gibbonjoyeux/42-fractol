/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   event_key.c                                        :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: shuertas <marvin@42.fr>                    +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2018/03/06 15:36:19 by shuertas          #+#    #+#             */
/*   Updated: 2018/03/06 16:03:13 by shuertas         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "fractol.h"

/*
********************************************************************************
*** STATIC FUNCTIONS ***********************************************************
********************************************************************************
*/

static void	key_fractal(t_fractol *fct, int32_t keycode)
{
	if (keycode == 18)
		set_fractal(fct, 0);
	else if (keycode == 19)
		set_fractal(fct, 1);
	else if (keycode == 20)
		set_fractal(fct, 2);
	else if (keycode == 21)
		set_fractal(fct, 3);
}

static void	key_zoom(t_fractol *fct, int32_t keycode)
{
	if (keycode == 13)
	{
		fct->pos_x += 0.5 * fct->domain - fct->domain / 2;
		fct->pos_y += 0.5 * fct->domain - fct->domain / 2;
		fct->domain *= ZOOM_COEF / 1.5;
	}
	else if (keycode == 12)
		fct->domain /= ZOOM_COEF / 1.5;
}

static void	key_move(t_fractol *fct, int32_t keycode)
{
	if (keycode == 124)
		fct->pos_x += fct->domain / MOVE_COEF;
	else if (keycode == 123)
		fct->pos_x -= fct->domain / MOVE_COEF;
	else if (keycode == 126)
		fct->pos_y -= fct->domain / MOVE_COEF;
	else if (keycode == 125)
		fct->pos_y += fct->domain / MOVE_COEF;
}

/*
********************************************************************************
*** PUBLIC FUNCTION ************************************************************
********************************************************************************
*/

int32_t		event_key(int32_t keycode, void *param)
{
	t_fractol	*fct;

	fct = (t_fractol*)param;
	if (keycode == 53)
		exit(0);
	key_fractal(fct, keycode);
	key_zoom(fct, keycode);
	key_move(fct, keycode);
	if (keycode == 49)
		fct->dynamic = !fct->dynamic;
	else
		fct->level = 0;
	return (0);
}
