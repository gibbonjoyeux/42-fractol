/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   event_clic.c                                       :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: shuertas <marvin@42.fr>                    +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2018/03/06 15:36:13 by shuertas          #+#    #+#             */
/*   Updated: 2018/03/06 16:03:14 by shuertas         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "fractol.h"

/*
********************************************************************************
*** STATIC FUNCTIONS ***********************************************************
********************************************************************************
*/

/*
********************************************************************************
*** PUBLIC FUNCTION ************************************************************
********************************************************************************
*/

int32_t		event_clic(int32_t button, int32_t x, int32_t y, void *param)
{
	t_fractol	*fct;

	fct = (t_fractol*)param;
	if (x < 0 || x > WINDOW_W || y < 0 || y > WINDOW_H)
		return (0);
	if (button == 1)
	{
		fct->pos_x += ((double)x / WINDOW_W) * fct->domain - fct->domain / 2.0;
		fct->pos_y += ((double)y / WINDOW_H) * fct->domain - fct->domain / 2.0;
		fct->domain *= ZOOM_COEF / 1.5;
	}
	else if (button == 4)
	{
		fct->pos_x += (((double)x / WINDOW_W) * fct->domain - fct->domain / 2.0)
		* 0.3;
		fct->pos_y += (((double)y / WINDOW_H) * fct->domain - fct->domain / 2.0)
		* 0.3;
		fct->domain *= ZOOM_COEF;
	}
	else if (button == 7)
		fct->domain /= ZOOM_COEF;
	fct->level = 0;
	return (0);
}
