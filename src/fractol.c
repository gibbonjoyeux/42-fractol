/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   fractol.c                                          :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: shuertas <marvin@42.fr>                    +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2018/03/06 15:36:02 by shuertas          #+#    #+#             */
/*   Updated: 2018/03/06 16:12:36 by shuertas         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "fractol.h"

/*
********************************************************************************
*** STATIC FUNCTIONS ***********************************************************
********************************************************************************
*/

static void	usage(char *message)
{
	if (message)
		ct_mes_failure(message);
	ct_putendl("./fractol [ mandelbrot | julia | ship | tunnel ]");
	ct_putendl("KEYS:");
	ct_putendl(". q / w ........................ ZOOM/DEZOOM");
	ct_putendl(". left / right / up / down ..... MOVE");
	ct_putendl(". 1 / 2 / 3 / 4 ................ CHANGE FRACTAL");
	ct_putendl("\t1 - Mandelbrot");
	ct_putendl("\t2 - Julia");
	ct_putendl("\t3 - Burning Ship");
	ct_putendl("\t4 - Tunnel");
	exit(0);
}

static void	init_window(t_mlx *mlx)
{
	int32_t	bpp;
	int32_t	line_length;
	int32_t endian;

	if (!(mlx->mlx = mlx_init()))
		ct_exit_fail("Cannot initialize mlx->");
	if (!(mlx->win = mlx_new_window(mlx->mlx, WINDOW_W, WINDOW_H, "fract'ol")))
		ct_exit_fail("Cannot initialize mlx window.");
	if (!(mlx->mlx_canvas = mlx_new_image(mlx->mlx, WINDOW_W, WINDOW_H)))
		ct_exit_fail("Cannot initialize mlx main canvas.");
	if (!(mlx->canvas = (uint8_t*)mlx_get_data_addr(mlx->mlx_canvas, &bpp,
	&line_length, &endian)))
		ct_exit_fail("Cannot initialize mlx main canvas.");
	if (!(mlx->mlx_target_canvas = mlx_new_image(mlx->mlx, 7, 7)))
		ct_exit_fail("Cannot initialize mlx target canvas.");
	if (!(mlx->target_canvas =
	(uint8_t*)mlx_get_data_addr(mlx->mlx_target_canvas, &bpp, &line_length,
	&endian)))
		ct_exit_fail("Cannot initialize mlx target canvas.");
	mlx_clear_window(mlx->mlx, mlx->win);
}

static void	init_events(t_fractol *fct)
{
	mlx_key_hook(fct->mlx.win, &event_key, fct);
	mlx_mouse_hook(fct->mlx.win, &event_clic, fct);
	mlx_hook(fct->mlx.win, 6, (1L << 6), &event_mouse, fct);
	mlx_loop_hook(fct->mlx.mlx, loop, fct);
}

static void	init_fractol(t_fractol *fct, char *name)
{
	uint8_t	i;

	if (!name)
		usage("Fractal name is not given.");
	i = 0;
	while (name[i] && i < 255)
	{
		name[i] = ct_tolower(name[i]);
		++i;
	}
	ct_printf("Trying to open [%s] fractal...\n", name);
	if (ct_strequ(name, "mandelbrot"))
		set_fractal(fct, 0);
	else if (ct_strequ(name, "julia"))
		set_fractal(fct, 1);
	else if (ct_strequ(name, "ship"))
		set_fractal(fct, 2);
	else if (ct_strequ(name, "tunnel"))
		set_fractal(fct, 3);
	else
		usage("Fractal name is not correct.");
}

/*
********************************************************************************
*** PUBLIC FUNCTION ************************************************************
********************************************************************************
*/

int32_t		main(int32_t ac, char **av)
{
	t_fractol	*fct;

	if (ac != 2)
		usage("Wrong number of argument.");
	if (!(fct = malloc(sizeof(t_fractol))))
		ct_exit_fail("Not enough memory.");
	init_fractol(fct, av[1]);
	init_window(&(fct->mlx));
	init_events(fct);
	init_target(fct);
	mlx_loop(fct->mlx.mlx);
	return (0);
}
