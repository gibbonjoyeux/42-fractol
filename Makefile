# **************************************************************************** #
#                                                                              #
#                                                         :::      ::::::::    #
#    Makefile                                           :+:      :+:    :+:    #
#                                                     +:+ +:+         +:+      #
#    By: shuertas <marvin@42.fr>                    +#+  +:+       +#+         #
#                                                 +#+#+#+#+#+   +#+            #
#    Created: 2016/11/21 14:10:28 by shuertas          #+#    #+#              #
#    Updated: 2018/03/06 16:14:55 by shuertas         ###   ########.fr        #
#                                                                              #
# **************************************************************************** #

C_GREEN 	= \033[32;01m
C_NO 		= \033[0m
C_RED 		= \033[31;01m
C_YELLOW 	= \033[33;01m
C_CYAN		= \033[36;01m
C_PINK		= \033[35;01m
C_BLACK 	= \033[30;01m

################################################################################
##[ SOURCES ]###################################################################
################################################################################

SRCS		= 	./src/fractol.c \
				./src/set_fractal.c \
				./src/event_key.c \
				./src/event_clic.c \
				./src/event_mouse.c \
				./src/init_target.c \
				./src/loop.c \
				./src/draw_init.c \
				./src/draw_precise.c \
				./src/draw_target.c \
				./src/fct_init_mandelbrot.c \
				./src/fct_precise_mandelbrot.c \
				./src/fct_init_julia.c \
				./src/fct_precise_julia.c \
				./src/fct_init_ship.c \
				./src/fct_precise_ship.c \
				./src/fct_init_tunnel.c \
				./src/fct_precise_tunnel.c \
				./src/color.c

OBJS		= $(SRCS:.c=.o)

NAME		= fractol
FLAGS		= -Wall -Wextra -Werror -g
CC			= gcc
INCLUDES	= -I ./inc/ -I ./minilibx/ -I ./libct/
LIB			= libct/libct.a -lmlx -framework OpenGL -framework AppKit

BIN_NUM		= 0
BIN_MAX		= $(shell echo $(OBJS) | wc -w)

################################################################################
##[ RULES ]#####################################################################
################################################################################

##############################
##[ MAIN RULE ]###############
##############################
all: $(NAME)

##############################
##[ LIBRARY RULE ]############
##############################
$(NAME): $(OBJS)
	@tput cuu1 ; tput cuf 1
	@printf "100"
	@tput cnorm
	@rm -rf .bin_num .bin_num_tmp .bin_state
	@printf "\n$(C_GREEN)SOURCES $(C_BLACK)[$(C_YELLOW)OK$(C_BLACK)]$(C_NO)\n"
	@make -C libct
	@make -C minilibx
	@gcc $(FLAGS) $(OBJS) $(LIB) -o $(NAME)
	@printf "$(C_GREEN)FRACT'OL $(C_BLACK)[$(C_YELLOW)DONE$(C_BLACK)]$(C_NO)\n"

##############################
##[ COMPILATION RULE ]########
##############################
%.o: %.c Makefile inc/fractol.h libct/full_libct.h libct/libct/libct.h
	@if [ -z ${INTRO} ]; then \
		tput civis; \
		printf "$(C_CYAN)[$(C_YELLOW)000 $(C_PINK)%%$(C_CYAN)]$(C_YELLOW)\n"; \
	fi
	$(eval INTRO := 1)
	@gcc $(FLAGS) $(INCLUDES) -o $@ -c $< || (tput cnorm && FAIL 2>&-)
	@$(eval BIN_NUM		:= $(shell echo "${BIN_NUM}+1" | bc))
	@$(eval BIN_STATE 	:= $(shell echo "${BIN_NUM}*100/${BIN_MAX}" | bc))
	@tput cuu1 ; tput cuf 1
	@printf "%.3d\n" ${BIN_STATE}

##############################
##[ CLEANING RULES ]##########
##############################
clean:
	@rm -rf $(OBJS)

fclean: clean
	@rm -rf $(NAME)

##############################
##[ RESTART RULES ]###########
##############################
re: fclean
	@make
