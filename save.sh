#!/bin/bash

C_RED="\033[31;01m"
C_GREEN="\033[32;01m"
C_YELLOW="\033[33;01m"
C_BLUE="\033[34;01m"
C_PINK="\033[35;01m"
C_CYAN="\033[36;01m"
C_NO="\033[0m"

#######################
 ### FLAGS + USAGE ###
#######################

usage()
{
	printf "${C_RED}%s\n" "$1"
	printf "${C_YELLOW}%s${C_BLUE} %s\n" "Usage:" "./save [-m] message"
	printf "${C_YELLOW}%s${C_BLUE} %s\n" "Help:" "Save the project with git"
	printf "\t- git add\n"
	printf "\t- git commit \"message\"\n"
	printf "\t- git push\n"
	printf "${C_YELLOW}%s\n" "Options:"
	printf "${C_GREEN}\t%s\t${C_NO}" "-m" "launch \"make fclean\" before \"git add\""
	exit 1
}

message=$1

##############
 ### MAIN ###
###############

success()
{
	printf "${C_CYAN}[${C_YELLOW}%s${C_CYAN}]${C_PINK} - ${C_GREEN}%s${C_NO}\n" "OK" "$1"
}

failure()
{
	printf "${C_RED}[${C_YELLOW}%s${C_RED}]${C_PINK} - ${C_RED}%s${C_NO}\n" "FAIL" "$1"
}

# GIT ADD
git add -A
if [ $? -ne 0 ]; then
	failure "CANNOT ADD"
	exit 1
fi
success	"ADDED EVERYTHING"

# GIT COMMIT
git commit -m "$message"
if [ $? -ne 0 ]; then
	failure "CANNOT COMMIT"
	printf "${C_CYAN}%s${C_NO}\n" "Do you want to push anyway ?"
	read
else
	success "COMMITED"
	printf "\"${C_PINK}%s${C_NO}\"\n" "$message"
fi

# GIT PUSH
git push -q
if [ $? -ne 0 ]; then
	failure "CANNOT PUSH"
	exit 1
fi
success "PUSHED"
